# LIO-SAM (Last Mile Project)

2022.03.08 Ver.

## 기억해야할 것들

* lio_sam_de로 패키지 명 변경 (Deepexpress의 하위 submodule로 추가됨)
* Last mile 시연할 때의 코드와 취합함
* rviz의 link 이름들 맞춤
* Optimization에 save하는 거 off함
* Post-processing은 fine-mapping 관련 레포에 있음!
* Optimization에 node를 바로 publish하게 수정 
    * 하지만laserHandler는 10Hz로 도네...?
	* 왜 5 Hz로 떨어지는 지 분석할 필요가 있음


---

## Run the package

1. Run the launch file:
```
roslaunch lio_sam_de run.launch
```

2. Play existing bag files:
```
rosbag play 2021-10-12-11-10-27_bongeunsa.bag -r 2
```

# Extrinsics

lidar to IMU가 필요함 (pose 관점에서 lidar w.r.t world의 오른쪽에 곱하면 IMU w.r.t world가 됨)

`ht_params.yaml`에 세팅 다 되어 있음

### extrinsicTrans
Depend on the sensor setting, `extrinsicTrans` 수정해주어야 함

**22/03/06:** The parameter of Ouster-128 is added
* For Vel-16 on the front side,
``` 
extrinsicTrans: [-0.061, 0.005, -0.328]
```
* For Vel-16 on the top side (but deprecated),
``` 
extrinsicTrans: [-0.005, 0.021, -0.368]
```
* For Ouster-128 on the front side,
``` 
extrinsicTrans: [-0.005, 0.021, -0.3518]
```

### extrinsicLiDAR2BodyTrans

pose 오른쪽 term에 `lidar2sensorBox`를 곱하면 pose가 LiDAR 기준 -> Sensor Box 기준이 된다는 의미에서 `lidar2sensorBox`라 칭함

(registration의 src2target tf와는 상반된 표기임!!)

        
* For Vel-16 on the front side,
``` 
extrinsicTrans: [0.406, 0.000, 0.344]
```
* For Vel-16 on the top side (but deprecated),
``` 
extrinsicTrans: [0.350, -0.016, 0.384]
```
* For Ouster-128 on the front side,
``` 
// OS-128: lower than 0.0162 m than Z value of Vel-16
extrinsicTrans: [0.350, -0.016, 0.3678]
```


- 주의할 점
1. 파일 저장 공간이 기본적으로 "HOME/$username$" + 뭐시기뭐시기/ 로 지정되어 있음. 마지막 / 꼭 필요
2. 저장하고 싶을 때는

```jsx
rosservice call /lio_sam/save_map "resolution: 0.0
destination: ''"
```

# Save mapping

sensor box 기준으로 저장해야 하기 때문에, `mapOptimization.cpp` 내에서 sensor box 기준으로 돌림

- 그리고 고려대측에서 map align이 되었으면 좋겠다고 하여 align 다시 해서 뽑는 중 (첫번째 lidar pose를 원점 비스무리하게 사용함. 정확히 원점은 아님. 곱한 후에 lidar2box를 다시 곱하기 때문. lidar2box가 orthogonal해서 괜찮을 것으로 보임)
- 🥵 현재는 pose들이 **target lidar 기준**이기 때문에, 어떤 lidar를 쓰냐에 따라서 **lidar2sensorbox** 제대로 지정해줘야 됨!!

# Localization에 쓰려면?

현재 lidar frame 기준의 pose를 tf broadcaster로 쏘고 있음. (launch 실행시키면 rviz가 바로 보여서 이해될 것임)

따라서 이 tf를 MCL에서 subscribe한 후에 상황에 맞는 lidar2sensorbox 오른쪽에 곱해주어서 pose transformation을 한 후에 써야 함

## NOTE (For 승원)

* `/lio_sam/lastmile/node_in_image_projection`

    * Image Projection에서 deskewing 후에 IMU odom의 initial guess를 pose로 해서 노드 생성

* `/lio_sam/lastmile/node`

    * Submap에 registration을 한 후 pose correction이 된 optimized pose를 `geometry_msgs::Pose`화해서 노드 생성
    * 정확한 건 아래 노드가 훨씬 정확함!
* **News (22/03/08, 저장 실패 후) **
    * `config/ht_params.yaml`의 `last_mile: saveRawPointCloud`를 `true`로 설정해야 keyframe이 저장됨
    * Demo할 때 쓸데없이 메모리 사용하는 것을 방지하기 위해 flag화해뒀음

